# tingle's .vimrc

## install .vimrc:

`git clone https://gitlab.com/wheelsandbytes/vimrc.git`

`cd vimrc`

`cp vimrc ~/.vimrc`

## install [Vundle](https://github.com/VundleVim/Vundle.vim) if not already installed:

`git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`

Launch `vim` and run `:PluginInstall`

or

To install from command line: `vim +PluginInstall +qall`

## install [vim-colors-solarized](https://github.com/altercation/vim-colors-solarized)

`mkdir ~/.vim/colors`

`git clone https://github.com/altercation/vim-colors-solarized.git`

`cd vim-colors-solarized/colors`

`mv solarized.vim ~/.vim/colors/`