" set number " show line numbers
set relativenumber " relative line numbers
set autoread " watch for file changes
set nocompatible " required for plugins to work
" set cursorline " highlight line on which the cursor is on
set tabstop=4
set softtabstop=2
set shiftwidth=2
set autoindent
set ignorecase
set ruler " show the line number on the bar
set showmode
set showcmd
set nowrap
set expandtab
syntax enable
" set colorcolumn=80
filetype off

au BufNewFile,BufRead *.md set filetype=markdown
au BufNewFile,BufRead *.sls set filetype=yaml
au BufNewFile,BufRead *.yml set filetype=yaml

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
Plugin 'altercation/solarized'
Plugin 'tomasr/molokai'
Plugin 'mxw/vim-jsx'
Plugin 'scrooloose/nerdtree'
Plugin 'mattn/webapi-vim'
Plugin 'mattn/gist-vim'
Plugin 'tpope/vim-fugitive'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'airblade/vim-gitgutter'
Plugin 'fatih/vim-go'
Plugin 'pangloss/vim-javascript'
Plugin 'heavenshell/vim-jsdoc'
Plugin 'wavded/vim-stylus'
Plugin 'vim-scripts/nagios-syntax'
Plugin 'elzr/vim-json'
Plugin 'Yggdroot/indentLine'
Plugin 'w0rp/ale'
Plugin 'ruanyl/vim-fixmyjs'
Plugin 'chriskempson/base16-vim'
Bundle 'djoshea/vim-autoread'

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


" airline
let g:airline_theme='molokai'
let g:airline_powerline_fonts = 1

" javscript jsx syntax highlighting
let g:jsx_ext_required = 0

" jsdoc highlighting
let g:javascript_plugin_jsdoc = 1

" jsdoc & es6
let g:jsdoc_enable_es6 = 1

" ale
let g:jsx_ext_required = 0
let g:ale_lint_on_save = 1
let g:ale_lint_on_text_changed = 0
let g:ale_linters = { 'javascript': ['eslint'] }
" let g:ale_javascript_eslint_options = '--fix'

" gist bundle config
let g:gist_detect_filetype = 1

" better file explorer
let g:netrw_liststyle=3

" autocomplete
set completeopt-=preview

" color scheme config
" set background=light
" colorscheme molokai
" let g:molokai_original = 1

" set background=dark
let g:solarized_termcolors=256
colorscheme solarized

" set termguicolors
" colorscheme base16-default-dark

"go-vim settings (see https://github.com/fatih/go-vim-tutorial)
" let g:go_fmt_command = "goimports"
" let g:go_metalinter_enabled = ['vet', 'golint'] ", 'errcheck']
" let go_metalinter_autosave = 1

" nerdtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" focus keymapping
map <C-L> :NERDTreeToggle<CR>
map <C-l> :NERDTreeFocus<CR>
let g:NERDTreeShowHidden = 1
let g:NERDTreeIgnore = ['\.sw[op]$','^\.DS']

" DISABLE THEM ARROW KEYS
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" auto line number switching
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set number norelativenumber
augroup END
